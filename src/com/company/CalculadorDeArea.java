package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CalculadorDeArea {

    Circulo circulo;
    Retangulo retangulo;
    Triangulo triangulo;

    public void calcularArea(List<Double> lados) {

        if (lados.size() == 1) {
            circulo = new Circulo(lados);
            circulo.calcularArea();

        } else if (lados.size() == 2) {
            retangulo = new Retangulo(lados);
            retangulo.calcularArea();

        } else if (lados.size() == 3) {
            triangulo = new Triangulo(lados);
            triangulo.calcularArea();
        } else {
            System.out.println("Triangulo Invalido!!!");

        }

    }

    public Double obterAreaCirculo() {
        return circulo.getArea();
    }

    public Double obterAreaRetangulo() {

        return retangulo.getArea();
    }

    public Double obterAreaTriangulo() {
        return triangulo.getArea();
    }

    public void solicitarCalculoDeArea(int quantidadeDeLados, CalculadorDeArea calculadorDeArea) {
        List<Double> lados = new ArrayList<>();
        Scanner sc1 = new Scanner(System.in);

        for (int i = 0; i < quantidadeDeLados; i++) {

            System.out.print("Digite o valor da medida: \n");
            double valorDigitado = sc1.nextDouble();

            lados.add(valorDigitado);
        }
        calculadorDeArea.calcularArea(lados);
    }

}
