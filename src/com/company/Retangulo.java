package com.company;

import java.util.List;

public class Retangulo extends FormaGeometrica {

    private Double ladoA;
    private Double ladoB;
    private Double area;

    public Retangulo(List<Double> lados) {
        this.ladoA = lados.get(0);
        this.ladoB = lados.get(1);
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Override
    public void calcularArea() {

        this.area = (ladoA * ladoB);
    }
}
