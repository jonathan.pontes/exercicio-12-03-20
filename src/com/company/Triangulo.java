package com.company;

import java.util.List;

public class Triangulo extends FormaGeometrica {

    private Double ladoA;
    private Double ladoB;
    private Double ladoC;
    private Double area;

    public Triangulo(List<Double> lados) {
        this.ladoA = lados.get(0);
        this.ladoB = lados.get(1);
        this.ladoC = lados.get(2);
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Override
    public void calcularArea() {
        if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true){
            double s = (ladoA + ladoB + ladoC) / 2;
            this.area = Math.sqrt(s * (s - ladoA) * (s - ladoB)) * (s - ladoC);
        }else{
            System.out.println("ERRO TRIANGULO NAO VALIDO");
        }

    }
}
