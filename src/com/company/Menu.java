package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {

    public void montarMenu(CalculadorDeArea calculadorDeArea) {
        int opcao;
        Scanner entrada = new Scanner(System.in);
        List<String> listaDeMensagens = new ArrayList<>();

        do {
            System.out.println("\n-----------------------------------------------------");
            System.out.println("Escolha uma forma geometrica para calcular a area:" +
                    "\n1 - Para Circulo " +
                    "\n2 - Para Quadrado " +
                    "\n3 - Para Triangulo " +
                    "\n0 - Para Sair");
            System.out.println("\n-----------------------------------------------------");

            opcao = entrada.nextInt();

            if (opcao == 1) {
                //Circulo
                calculadorDeArea.solicitarCalculoDeArea(1, calculadorDeArea);
                escreverMensagemFinal("A area circulo é: " + calculadorDeArea.obterAreaCirculo(), listaDeMensagens);

            } else if (opcao == 2) {
                //Quadrado
                calculadorDeArea.solicitarCalculoDeArea(2, calculadorDeArea);
                escreverMensagemFinal("A area do quadrado é: " + calculadorDeArea.obterAreaRetangulo(), listaDeMensagens);

            } else if (opcao == 3) {

                //Triangulo
                calculadorDeArea.solicitarCalculoDeArea(3, calculadorDeArea);
                escreverMensagemFinal("A area do triangulo é: " + calculadorDeArea.obterAreaTriangulo(), listaDeMensagens);

            } else {
                System.out.println("Obrigado Volte Sempre! ");
            }
        } while (opcao != 0);
    }

    private static void escreverMensagemFinal(String mensagemParcial, List<String> listaDeMensagens) {

        listaDeMensagens.add(mensagemParcial);
        for (int i = 0; i < listaDeMensagens.size(); i++) {
            System.out.println(listaDeMensagens.get(i));
        }
    }
}
