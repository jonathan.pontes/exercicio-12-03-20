package com.company;

import java.util.List;

public class Circulo extends FormaGeometrica {

    public Double ladoA;
    private Double area;

    public Circulo(List<Double> lados) {

        this.ladoA = lados.get(0);
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    @Override
    public void calcularArea() {
        this.area = Math.pow(ladoA, 2) * Math.PI;
    }


}
